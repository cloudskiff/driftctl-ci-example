provider "aws" {
  region = "us-east-1"
}

resource "random_string" "random" {
  length  = 8
  lower   = true
  upper   = false
  number  = false
  special = false
}

resource "aws_s3_bucket" "my-bucket" {
  bucket = "${random_string.random.result}-gitlabci-dctl"
}
